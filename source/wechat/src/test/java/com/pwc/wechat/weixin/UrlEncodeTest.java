package com.pwc.wechat.weixin;

import java.io.UnsupportedEncodingException;

import org.junit.Test;

import com.pwc.wechat.util.HttpUtil;

public class UrlEncodeTest {
	
	@Test
	public void testEncode() {
		try {
			String url = HttpUtil.toRedirectUrl("http://www.baidu.com", "");
			System.out.println(url);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
