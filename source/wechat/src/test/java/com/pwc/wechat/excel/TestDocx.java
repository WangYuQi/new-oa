package com.pwc.wechat.excel;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.ICandidatePhotoCrud;
import com.pwc.wechat.entity.candidate.CandidatePhoto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class TestDocx {
	@Autowired
	private ICandidatePhotoCrud iCandidatePhotoCrud;
	
	@Test
	public void test() throws IOException {
		Path docFile = Paths.get("D:/PrettyDocxFormat.docx");
		XWPFDocument document = new XWPFDocument(Files.newInputStream(docFile));
		document.getTables().forEach(table->{
			System.out.println("---table---");
			table.getRows().forEach(row->{
				System.out.println("---row---");
				row.getTableCells().forEach(cell->{
					System.out.println("---cell---");
					System.out.println(cell.getText());
				});
			});
		});
		document.close();
	}
	
	@Test
	@Commit
	@Transactional(readOnly = true)
	public void test2() throws IOException{
		Path docFile = Paths.get("C:/users/wwang238/PrettyDocxFormat.docx");
		XWPFDocument document = new XWPFDocument(Files.newInputStream(docFile));
		document.getTables().forEach(table->{
			table.getRows().forEach(row->{
				row.getTableCells().forEach(cell->{
					if(cell.getText().equals("image")){
						cell.removeParagraph(0);
						XWPFParagraph para = cell.addParagraph();
						XWPFRun run = para.createRun();				
				
						System.out.println("==========");
						CandidatePhoto photo = iCandidatePhotoCrud.findOne("121212199109012211");
						System.out.println("==========");
						InputStream sbs=new ByteArrayInputStream(photo.getImage());
						try {
							OutputStream outputStream = Files.newOutputStream(Paths.get("C:/logs/aa.jpeg"));
							outputStream.write(photo.getImage());
							run.addPicture(sbs,Document.PICTURE_TYPE_PNG, "image", Units.toEMU(85),Units.toEMU(90));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			});
		});
		document.write(Files.newOutputStream(Paths.get("C:/logs/aa.docx")));
		document.close();
	}
	@Test
	public void test3() throws IOException{
		Path docFile = Paths.get("C:/users/wwang238/PrettyDocxFormat.docx");
		CandidatePhoto photo = iCandidatePhotoCrud.findOne("640302199410123333");
		XWPFDocument document = new XWPFDocument(Files.newInputStream(docFile));
		try {
			document.addPictureData(photo.getImage(), 1);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
	}

}
