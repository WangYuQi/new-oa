package com.pwc.wechat.test.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.hr.IUserCrud;
import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.entity.user.UserRole;
import com.pwc.wechat.service.hr.user.IUserHandler;
import com.pwc.wechat.service.hr.user.IUserRoleHandler;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration

public class HrmServiceTest {
	
	@Autowired
	private IUserHandler iUserHandler;
	
	@Autowired
	private IUserRoleHandler iUserRoleHandler;
	
	@Autowired
	private IUserCrud iUserCrud;
	
	@Test
	@Transactional
	public void addUserTest(){
		User user = new User();
		user.setUserName("nameForTest");
		user.setPassword("1234");
		user.setEnabled(true);
		iUserHandler.insertUser(user);	
		assertTrue("user is enabled",iUserCrud.findOne("nameForTest").getEnabled());		
	}
	
	
    @Test
    @Transactional
    public void verifyPasswordTest(){
    	addUserTest();
    	assertEquals("userName equal",iUserCrud.findOne("nameForTest").getUserName(),"nameForTest"); 
		User user = new User();
		user.setUserName("nameForTest");
		user.setPassword("1234");
    	assertTrue("password should be 1234",iUserHandler.verifyPassword(user));
    }
    
    @Test
    @Transactional
    public void changePasswordTest(){
    	addUserTest();
    	verifyPasswordTest();
    	User user = new User();
		user.setUserName("nameForTest");
		user.setPassword("5678");
        assertFalse("password should be 1234",iUserHandler.verifyPassword(user));     
        iUserHandler.changePassword(user);
        assertTrue("password should be 5678",iUserHandler.verifyPassword(user));
    }
}
