package com.pwc.wechat.weixin;

import org.junit.Test;

import com.pwc.wechat.entity.weixin.Message;
import com.pwc.wechat.util.XmlUtil;

public class XmlTest {

	@Test
	public void testXml (){
		Message message = new Message();
		message.setContent("cccc");
		message.setCreateTime(123456L);
		message.setFromUserName("ooooo");
		message.setMsgId(3666L);
		message.setMsgType("txt");
		message.setToUserName("wechat");
		String res = new String(XmlUtil.toNormalXml(message));
		System.out.println(res);
	}
}
