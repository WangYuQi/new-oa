package com.pwc.wechat.excel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.service.upload.ImportFromExcel;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class TestInsertFromExcel {
	
	@Autowired
	private ImportFromExcel importFromExcel;
	
	@Test
	public void testInsert() throws IOException {
		Path xlsxFile = Paths.get("D:/talent.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(xlsxFile));
//		importFromExcel.readExcelAndSave(workbook);
		workbook.close();
	}

}
