package com.pwc.wechat.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class HibernateTest {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Test
	@Commit
	@Transactional(readOnly = false)
	public void testHibernateTemplate(){
		Application app = new Application();
		app.setNationalId("310105101010100001");
		app.setApplyId(123456);
		app.setApplyDate(DateUtil.getLocalDate());
		app.setApplyPosition("Dev");
		hibernateTemplate.save(app);
	}
	
	@Test
	@Rollback
	@Transactional(readOnly = false)
	public void testJpa(){
		Candidate app = new Candidate();
		app.setNationalId("777788");
		app.setCellPhone("rrr");
		app.setCurrentProvince("ee");
		app.setName("eee");
		app.setBirthday("20120202");
		app.setGender("male");
		app.setMailAddress("rrr");
		hibernateTemplate.save(app);
	}

	

	
	@Test
	@Commit
	@Transactional(readOnly = false)
	public void testSession(){
		Application app = new Application();
		app.setNationalId("310105101010100001");
		app.setApplyId(123456);
		app.setApplyDate(DateUtil.getLocalDate());
		app.setApplyPosition("Dev");
		sessionFactory.getCurrentSession().save(app);
	}
	
	@Test
	@Commit
	public void testQuery(){
		List<Application> res = hibernateTemplate.findByExample(new Application());
		System.out.println(res.size());
	}
		
		
}
