package com.pwc.wechat.weixin;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import com.pwc.wechat.service.weixin.AccessVerify;

public class AccessVerifyTest {
	

	@Test
	public void test() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		AccessVerify accessVerify = new AccessVerify();
		String signature = "1f29e41b6a4ea11e7cf9a5f8ecc35bb6d5b31a9a";
		String timestamp = "1464944934";
		String nonce = "1777115444";
		String echostr = "7451262433279433206";
		String reString = accessVerify.excuteVerify(signature, timestamp, nonce, echostr);
		System.out.println(reString);
	}
	
	@Test
	public void test0xFF () {
		int a = 8 & 0xFF;
		System.out.println(a);
	}

}
