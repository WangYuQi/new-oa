package com.pwc.wechat.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class ApplicationTest {

	
	@Commit
	@Test
	public void testInsert() {
		Application app = new Application();
		app.setNationalId("66");
		app.setApplyId(2);
		app.setApplyDate(DateUtil.getLocalDate());
		
		
		Candidate abb = new Candidate();
		abb.setNationalId("66");
		abb.setCellPhone("rrr");
		abb.setCurrentProvince("ee");
		abb.setName("ewe");
		abb.setBirthday("20120202");
		abb.setGender("male");
		abb.setMailAddress("rrr");
		
		
		}
	
	@Commit
	@Test
	public void testUpdate() {
		Application app = new Application();
		app.setNationalId("31010520160101000X");
		app.setApplyId(123456);
		app.setApplyDate(DateUtil.getLocalDate());
	}
	
	@Commit
	@Test
	public void testFind() {
		Application app = new Application();
		app.setApplyDate("2016-04-24");
		app.setApplyId(123456);
	}
	
}
