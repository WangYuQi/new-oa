package com.pwc.wechat.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Candidate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class PagingAndSortingRepositoryTest {
	
	@Autowired
	private PagingAndSortingRepository<Candidate, String> pagingAndSortingRepository;
	
	@Test
	public void test() {
		Candidate one = pagingAndSortingRepository.findOne("310105101010100001");
		System.out.println(one.getName());
	}
	
}
