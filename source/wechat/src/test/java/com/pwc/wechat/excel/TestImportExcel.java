package com.pwc.wechat.excel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

public class TestImportExcel {

	@Test
	public void test() throws IOException {
		Path xlsxFile = Paths.get("D:/talent.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(xlsxFile));
		System.out.println(workbook.getNumberOfSheets());
		System.out.println(workbook.getSheetAt(0).getRow(1).getCell(0));
		workbook.close();
	}
	
	@Test
	public void testExcel() throws IOException{
		Path xlsxFile = Paths.get("C:/Users/wwang238/Downloads/Talent Pool.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(xlsxFile));
		String languageLevel=workbook.getSheetAt(0).getRow(4).getCell(18).getStringCellValue();
//		System.out.println("toString" + cell.toString());
//		System.out.println("getRichStringCellValue" + cell.getRichStringCellValue().getString());
		System.out.println(formateLanguageLevel(workbook.getSheetAt(0).getRow(4).getCell(18).getStringCellValue()));
//		
//		System.out.println(languageLevel.contains("CET-4"));
//		
//		System.out.println(languageLevel.contains("TEM-8"));
		
		
		
		
		
		
		
		
		workbook.close();
	}
	
	
	private String formateLanguageLevel(String languageLevel) {
		String[] formatLevel=new String[8];
		int i=0;
		String formatlanguagelevel="";
		String level[]=languageLevel.split(",");
		for (String rightlevel:level){
			switch (rightlevel) {
			case "CET-4":
				formatLevel[0]="CET-4";
				break;
			case "CET-6":
				formatLevel[1]="CET-6";
				break;
			case "TEM-4":
				formatLevel[2]="TEM-4";
				break;
			case "TEM-8":
				formatLevel[3]="TEM-8";
				break;
			case "JPT-1":
				formatLevel[4]="JPT-1";
				break;
			case "JPT-2":
				formatLevel[5]="JPT-2";
				break;
			case "JPT-3":
				formatLevel[6]="JPT-3";
				break;
			default:
				if(formatLevel[7]!=null){
				formatLevel[7]=formatLevel[7]+","+rightlevel;
				}else{
					formatLevel[7]=rightlevel;
				}
				break;
			}
		}
		for(String finalLevel:formatLevel){
			if(finalLevel!=null){
			formatlanguagelevel=formatlanguagelevel+finalLevel+",";}
			else{
				formatlanguagelevel=formatlanguagelevel+",";
			}
		}
		return formatlanguagelevel.toUpperCase();
	}

	
	
//	private String formateLanguageLevel(String languageLevel) {
//		List<String>formatLevel=new ArrayList<String>();
//		int i=0;
//		String formatlanguagelevel=null;
//		String[] level=languageLevel.split(",");
//		for (String rightlevel:level){
//			switch (rightlevel) {
//			case "CET-4":
//				formatLevel.add(0,"CET-4");
//				break;
//			case "CET-6":
//				formatLevel.add(1,"CET-6");
//				break;
//			case "TEM-4":
//				formatLevel.add(2,"TEM-4");
//				break;
//			case "TEM-8":
//				formatLevel.add(3,"TEM-8");
//				break;
//			case "JPT-1":
//				formatLevel.add(4,"JPT-1");
//				break;
//			case "JPT-2":
//				formatLevel.add(5,"JPT-2");
//				break;
//			case "JPT-3":
//				formatLevel.add(6,"JPT-3");
//				break;
//			default:
//				formatLevel.add(7+i,rightlevel);
//				i++;
//				break;
//			}
//		}
//		for(String finalLevel:formatLevel){
//			if(finalLevel!=null){
//			formatlanguagelevel=formatlanguagelevel+finalLevel+",";}
//			else{
//				formatlanguagelevel=formatlanguagelevel+",";
//			}
//				
//		}
//		return formatlanguagelevel;
//	}
//	

}
