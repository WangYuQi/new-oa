package com.pwc.wechat.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.CandidatePagingAndSorting;
import com.pwc.wechat.entity.candidate.Candidate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class PagingAndSorting {
	
	@Autowired
	private CandidatePagingAndSorting candidatePagingAndSorting;
	
	@Test
	public void testFindOne() {
		Candidate one = candidatePagingAndSorting.findOne("310105101010100001");
		System.out.println(one.getName());
	}
	
	@Test
	public void testPaging() {
		PageRequest pageable = new PageRequest(2,3);
		Page<Candidate> page = candidatePagingAndSorting.findAllByGenderAndWechatUid("male", "3084226", pageable);
		System.out.println("第" + (page.getNumber()+1) + "页");
		System.out.println("总共" + page.getTotalPages() + "页");
		System.out.println("总共" + page.getTotalElements() + "个元素");
		System.out.println("每页" + page.getSize() + "个元素");
		System.out.println("此页有" + page.getNumberOfElements() + "个元素");
		page.forEach((Candidate candidate)->{
			System.out.println(candidate.getName());
		});
	}
	
	@Test
	public void testFindAll() {
		candidatePagingAndSorting.findAll().forEach((Candidate candidate)->{
			System.out.println(candidate.getName());
		});
	}

}
