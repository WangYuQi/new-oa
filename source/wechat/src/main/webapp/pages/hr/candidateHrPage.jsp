<!DOCTYPE html>
<html data-ng-app="candidatePage">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- 上面三个meta必须在最前，其他都写在后面 -->
	<meta name="description" content="com.pwc.wechat">
	<meta name="author" content="WHO">
	<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
	<link href="favicon.ico" type="image/x-icon" rel=icon>
	<link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
	<title>Candidate Page</title>
	<!-- Bootstrap core CSS -->
	<link rel='stylesheet' type="text/css" href="/wechat/resources/css/bootstrap.min.css"/>
	<link rel='stylesheet' type="text/css" href="/wechat/resources/css/bootstrap-theme.min.css"/>
	<!-- Custom styles for this template -->
	<link rel='stylesheet' type="text/css" href="/wechat/resources/css/starter-template.css"/>
	<link rel='stylesheet' type="text/css" href="/wechat/resources/css/font.css"/>
	<link rel='stylesheet' type="text/css" href="/wechat/resources/css/WHO_CSS.css"/>
</head>
<body data-ng-controller="candidateController">
	<div class="container">
	<form name="candidateValidate" novalidate >
		<div class="page-header" style="text-align:center; margin: 10px 0 20px;"></div>
		<div class="list-group" >
			<a href="##" class="list-group-item list-group-item-warning	">
				<b>Position information</b>
			</a>
			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<b>Apply Position:</b>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<select data-ng-model="applicationPosition" class="form-control" style="width: 100px;">
									<option>HR</option>
									<option>GW</option>
									<option>JAVA</option>
									<option>.NET</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<b>Available Day:</b>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<select data-ng-model="candidate.availableSchedule" class="form-control" style="width: 100px;">
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		<div class="list-group">
			<a href="##" class="list-group-item list-group-item-warning	"> <b>Personal information</b>
			</a>
			<div class="table-responsive">			
				<table class="table table-condensed">
					<tbody>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="name">Name:</label>
							</td>
							<td style="width: 30px">
								<input data-ng-model="candidate.name" name="candidateName" autofocus="autofocus" required validateblur style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;" >
								<span style="color:red" ng-show="candidateValidate.candidateName.$error.required && candidateValidate.candidateName.$blur">Name is required</span>
								</td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="address">Province Now:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.currentProvince" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;"> <b>Gender&nbsp;&nbsp;&nbsp;:</b>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<input data-ng-model="candidate.gender" type="radio"  value="male" >
								male&nbsp;&nbsp;&nbsp;
								<input data-ng-model="candidate.gender" type="radio"  value="female" >female</td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="nationality">Nationality:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.nationality" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="IDNumber">ID Number:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.nationalId" name="IDnumber" required validateblur ng-pattern="/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
								<p style="color:red" ng-show="candidateValidate.IDnumber.$error.required && candidateValidate.IDnumber.$blur">ID Number is required</p>
								<p style="color:red" ng-show="candidateValidate.IDnumber.$error.pattern && candidateValidate.IDnumber.$blur">This is not a valid ID Number</p>
								</td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="passportcardnumber">Passport NO:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.passportNumber" name="passportNumber" validateblur ng-pattern="/^[a-zA-Z]{5,17}$|^[a-zA-Z0-9]{5,17}$/" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
								<p style="color:red" ng-show="candidateValidate.passportNumber.$blur && candidateValidate.passportNumber.$error.pattern">This is not a valid Passport Number</p>
								</td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="birthday">Birthday:</label>
							</td>
							<td style="width: 150px">
								<input type="date" "candidate.birthday" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="birthProvince">BirthProvince:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.birthProvince" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="homePhone">Home Phone:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.homePhone" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="cellPhone">Cell Phone:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.cellPhone" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="mailAddress">E-mail&nbsp;&nbsp;:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.mailAddress" type="email"  style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff;font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="personalPage">PersonalPage:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.personalPage" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="skillsDescribe">Skills:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.skillsDescribe" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr>
							<td style="width: 50px;font-size: 12pt;">
								<label for="learningIntention">LearnIntention:</label>
							</td>
							<td style="width: 150px">
								<input data-ng-model="candidate.learningIntention" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
						</tr>
						<tr data-ng-init="languageLevel = [];">
							<td style="width: 50px;font-size: 12pt;">
								<b>Language Skill:</b>
							</td>
							<td style="width: 50px;font-size: 12pt;">
							</td>
						</tr>
						<tr>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[0]" data-ng-true-value="'CET-4'" data-ng-false-value="" type="checkbox" >CET-4</label>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[1]" data-ng-true-value="'CET-6'" data-ng-false-value="" type="checkbox" >CET-6</label>
							</td>
						</tr>
						<tr>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[2]" data-ng-true-value="'TEM-4'" data-ng-false-value="" type="checkbox" >TEM-4</label>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[3]" data-ng-true-value="'TEM-8'" data-ng-false-value="" type="checkbox" >TEM-8</label>
							</td>
						</tr>
						<tr>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[4]" data-ng-true-value="'JPT-1'" data-ng-false-value="" type="checkbox" >JPT-1</label>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[5]" data-ng-true-value="'JPT-2'" data-ng-false-value="" type="checkbox" >JPT-2</label>
							</td>
						</tr>
						<tr>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">
									<input data-ng-model="languageLevel[6]" data-ng-true-value="'JPT-3'" data-ng-false-value="" type="checkbox" >JPT-3</label>
							</td>
							<td style="width: 150px; font-size: 12pt;">
								<label class="checkbox-inline">Other:</label>
									<input data-ng-model="languageLevel[7]" style="width: 100px;border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#education" >Education:</a>
				</h4>
			</div>
			<div id="education" class="panel-collapse collapse">
				<div class="panel-body" data-ng-init="candidate.educationExperiences=[]">
					<div data-ng-repeat="eduExp in candidate.educationExperiences">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>University:</span></td>
									<td><input data-ng-model="eduExp.university"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Major:</span></td>
									<td><input data-ng-model="eduExp.major"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Period:</span></td>
									<td><input data-ng-model="eduExp.period"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>GPA:</span></td>
									<td><input data-ng-model="eduExp.gpa"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Degree:</span></td>
									<td><input data-ng-model="eduExp.degree"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div>
						<button data-ng-click="addEduExp()" class="btn btn-default" type="button" style="padding: 4px 10px;" >Add +</button>
						<button data-ng-click="dropEduExp()" class="btn btn-default" type="button" style="padding: 4px 10px;" >Drop -</button>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#workexperience">Work Experience:</a>
				</h4>
			</div>
			<div id="workexperience" class="panel-collapse collapse">
				<div class="panel-body" data-ng-init="candidate.workExperiences=[]">
					<div data-ng-repeat="workExp in candidate.workExperiences">
						<table class="table table-condensed">
							<tbody>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Company:</span></td>
									<td><input data-ng-model="workExp.company"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Position:</span></td>
									<td><input data-ng-model="workExp.position"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Period:</span></td>
									<td><input data-ng-model="workExp.period"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td style="width: 50px;font-size: 12pt;"><span>Details:</span></td>
									<td><input data-ng-model="workExp.details"
										style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div>
						<button data-ng-click="addWorkExp()" class="btn btn-default" type="button" style="padding: 4px 10px;" >Add +</button>
						<button data-ng-click="dropWorkExp()" class="btn btn-default" type="button" style="padding: 4px 10px;" >Drop -</button>
					</div>
				</div>
			</div>
		</div>
		
		</div>
		<div style="text-align:center;">
			<button class="btn btn-warning" type="button" style="padding: 6px 100px;" id="submit" data-toggle="modal" data-target="#confirm" ng-disabled="candidateValidate.candidateName.$invalid || candidateValidate.IDnumber.$invalid ||candidateValidate.passportNumber.$invalid">Submit</button>
			<br>
			<br></div>
	</form>
		<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" 
		               aria-hidden="true">X</button>
						<h4 class="modal-title" id="myModalLabel">Confirm your information and sign!</h4>
					</div>
					<form role="form" style="margin-bottom:0;" class="modal-tmp">
						<div class="modal-body">
							<h1 style="font-family: 'stxingkairegular';">{{signature}}</h1>
							<input data-ng-model="signature" placeholder="Please Sign:" type="text" class="form-control" required="required" autocomplete="on" value="" autofocus="autofocus"></div>
					</form>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#sure" data-ng-click="submitCandidate()" >Sure</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<div class="modal fade" id="sure" tabindex="-1" role="dialog" 
		    aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" 
		               aria-hidden="true">X</button>
						<h4 class="modal-title" id="myModalLabel">Submit Success!</h4>
					</div>
					<div class="modal-body">Submit Success!</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
	</div>
	<!-- /.container -->
	<!-- Bootstrap core JavaScript  ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="/wechat/resources/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="/wechat/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/wechat/resources/js/angular.min.js"></script>
	<script type="text/javascript" src="/wechat/resources/js/interninformation.js"></script>
	<script type="text/javascript" src="/wechat/resources/js/angular-cookies.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>