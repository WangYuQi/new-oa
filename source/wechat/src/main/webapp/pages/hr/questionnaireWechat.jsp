<!DOCTYPE html>
<html data-ng-app="questionnaire">
  
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="description" content="com.pwc.wechat">
    <meta name="author" content="WHO">
    <link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
    <link href="favicon.ico" type="image/x-icon" rel=icon>
    <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
    <title>Questionnaire Page</title>
    <!-- Bootstrap core CSS -->
    <link rel='stylesheet' type="text/css" href="/wechat/resources/css/weui.min.css" />
    <!-- Custom styles for this template -->
    <link rel='stylesheet' type="text/css" href="/wechat/resources/css/candidate.css" /> </head>

<body data-ng-controller="questionnaireController">
     <div class="container">
            <p class="weui_cells_title">Questionnaire:</p>
          <div class="weui_cells">
               <form name="candidateValidate" novalidate>
               <select data-ng-model="selectedLanguage" data-ng-options="item for item in languages" data-ng-change="changeLanguage()">
               </select>
                <div data-ng-repeat="questionnaire in questionnaires.questionnaireList">
                <div style="font-size:11pt;width:90%;padding-top:10px;margin:0 auto;border-top:1px solid #d9d9d9;">
                    <div data-ng-model="questionnaire.question">{{$index+1}}) {{questionnaire.question}}</div>
                    <div><textarea style="width:100%;font-size:16px;border-radius:5px;padding:10px;margin:10px 0;resize:none;" data-ng-model="questionnaire.answer"></textarea></div>
                </div>
                </div>
                <div class="weui_btn_area">
                    <input type="button" value="Submit" class="weui_btn weui_btn_warn" data-ng-click="sendQuestionnaire()"> </div>
               </form>
           </div>
      </div> 
      
      <div class="weui_dialog_alert" data-ng-show="dialogShow">
       <div class="weui_mask"></div>
        <div class="weui_dialog">
             <div class="weui_dialog_hd">
                 <strong class = "weui_dialog_title">Thank you</strong>
             </div>
             <div class="weui_dialog_bd">
                 <strong class = "weui_dialog_title">We will notify you soon</strong>
             </div>
             <div class="weui_dialog_ft">
                 <a class="weui_btn_dialog primary" data-ng-click="hideNotice()">OK</a>
             </div>
        </div>
      </div>
</body>
     <script type="text/javascript" src="/wechat/resources/js/angular.min.js"></script>
     <script type="text/javascript" src="/wechat/resources/js/questionnaire.js" charset="utf-8"></script>   
</html>