<!doctype html>
<html lang="zh" ng-app="index">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%String path=request.getContextPath(); %>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="x-frame-options" ,"ALLOW-FROM" />
<link rel="stylesheet" type="text/css" href="resources/css/normalize.css" />
<link rel="stylesheet" href="<%=path%>/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=path%>/resources/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=path%>/resources/css/buttons.css">
<link rel="stylesheet" href="<%=path%>/resources/css/metisMenu.min.css">
<link rel="stylesheet" type="text/css" href="<%=path%>/resources/css/default.css">
<link href='http://fonts.useso.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/font.css"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/starter-template.css"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/WHO_CSS.css"/>
<link rel="stylesheet" type="text/css" href="<%=path%>/resources/css/styles.css">
<!-- [if IE] -->
<!-- 		<script src="http://libs.useso.com/js/html5shiv/3.7/html5shiv.min.js"></script> -->
<!-- [endif]  -->
</head>
<body ng-controller="indexController">
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
          	<img src="<%=path%>/resources/images/logo.png">
          	<span class="">Talent Pool</span>
          </a>
        </div>
        <ul class="nav navbar-right navbar-signout">
        	<li><a href="">Sign out</a></li>
        </ul>
    </div>
    <div class="side-content">
	    <nav nav-focus id="menu" class="navbar-default navbar-fixed-side" role="navigation">
	    	<div class="sidebar-collapse collapse in" aria-expanded="false">
	    		<ul class="nav" id="side-menu">
	    			<li class="sidebar-user">
	    				<div class="user-img">
	    					<span class="img-circle">
	    						<img src="<%=path%>/resources/images/user.jpg" class="img-circle img-responsive">
	    					</span>
	    				</div>
	    				<div class="user-info">
	    					<div class="user-name">EMILY YANG</div>
	    					<div class="user-role">MANAGER</div>
	    				</div>
	    			</li>
	    			<li class="on">
	    				<a href="indexpage">
	    					<i class="fa fa-users fa-fw"></i>
	    					Talent Pool
	    				</a>
	    			</li>
	    			<li>
	    				<a open-nav href="#" ng-click="toggle()">
	    					<i class="fa fa-search fa-fw"></i>
	    					Recruitment Process
	    					<i class="fa fa-angle-right angle-right" ng-show="!showSubnav"></i>
	    					<i class="fa fa-angle-down angle-down" ng-show="showSubnav"></i>
	    				</a>
	    				<ul id="process"class="nav nav-second-level collapse">
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q0-Campus Talk
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q1-Paper Test
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q2-Group 
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q3-Final Interview
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q4-Offer
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-angle-right fa-fw"></i>
	    							Q5-Intern Orientation
	    						</a>
	    					</li>
	    					<li>
	    						<a href="forHr/roundmover">
	    							<i class="fa fa-trash fa-fw"></i>
	    							Trash
	    						</a>
	    					</li>
	    				</ul>
	    			</li>
	    			<li class="">
	    				<a href="#" >
	    					<i class="fa fa-file-text fa-fw"></i>
	    					Sheets
	    				</a>
	    			</li>
	    			<li class="">
	    				<a href="#" >
	    					<i class="fa fa-user-plus  fa-fw"></i>
	    					Add Candidate
	    				</a>
	    			</li>
	    			<li class="">
	    				<a href="#" >
	    					<i class="fa fa-question-circle fa-fw"></i>
	    					Help
	    				</a>
	    			</li>
	    		</ul>
	    	</div>
	    </nav>	
		<div id="printContainer" class="content">
			<div class="container">
				<div>
					<select  ng-model="sort" ng-options="x for x in selections" ng-change="select()">
						<option value="">Please Select</option>
					</select>
					<button ng-click="disorder()">Disorder</button>
				</div>
				<table class="table table-hover" id="tab1">
					<thead>
						<tr>
							<th style="width: 20px">Name</th>
							<th style="width: 20px">Cell Phone</th>
							<th style="width: 20px">Position</th>
							<th style="width: 20px"></th>
							<th style="width: 50px">University</th>
							<th style="width: 20px">Status</th>
							<th style="width: 20px">Apply Date</th>
							<th style="width: 20px"></th>
							<th style="width: 20px"></th>
						</tr>
							<tr data-ng-repeat="item in items">
								<td data-ng-bind="item.name"></td>
								<td data-ng-bind="item.cell_phone"></td>
								<td data-ng-bind="item.apply_position"></td>
								<td></td>
								<td data-ng-bind="item.university"></td>
								<td data-ng-bind="item.status"></td>
								<td data-ng-bind="item.apply_date"></td>
								<td></td>
						</tr>
					</thead>
				</table>
				<center><input type="button" onclick="print()" value="Print"/></center>
			</div>
		</div>
	</div>
	    	<!-- <iframe  id="page" name="iframe_info" width=100% height="1000px" frameborder=0 src="forHr/allcandidate"></iframe> -->
	<script src="http://libs.useso.com/js/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
	<script src="<%=path%>/resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/resources/js/button.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/resources/js/metisMenu.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/resources/js/jquery.nicescroll.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=path%>/resources/js/angular.min.js"></script>
	<script type="text/javascript" src="<%=path%>/resources/js/angular-ui-router.js"></script>
	<script type="text/javascript" src="<%=path %>/resources/js/allcandidate.js"></script>
	<script type="text/javascript" src="<%=path %>/resources/js/jquery.jqprint.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
	<script language="javascript">
	   function print(){
	    	$("#printContainer").jqprint();
	   }
  	</script>
	<script type="text/javascript">
		$(function () {
    		$('#side-menu').metisMenu();
    		$( '#menu' ).niceScroll({
    				cursorcolor: '#fff',
    				railalign: 'right', 
    				cursorborder: "none", 
    				horizrailenabled: false, 
    				zindex: 2001, 
    				cursoropacitymax: 0, 
    				spacebarenabled: false 
    		});
    		$(window).bind("resize", function() {
    		     if ($(this).width() < 768) {
    		        $('div.sidebar-collapse').addClass('collapse')
    		    } else {
    		        $('div.sidebar-collapse').removeClass('collapse')
    		    }
    		});
    		$('.navbar-toggle').on('click', function () {
    		        if ($('#menu.hidden-xs').length){
    		            $('#menu').removeClass('hidden-xs');
    		        }
    		        else{
    		            $('#menu').addClass('hidden-xs');
    		        }
    		    });
  		});
	</script>
	<script type="text/javascript">
		// var app = angular.module('index', ['ui.router']);
		myModule.directive('openNav', function(){
			return {
				restrict: 'A', 
				link: function(scope, element, attrs) {
					scope.showSubnav = false;
					scope.toggle = function(){
						scope.showSubnav = !scope.showSubnav;
					}				
				}
			};
		});
		myModule.directive('navFocus', function(){
			return {
				restrict: 'A', 
				link: function(scope, element, attrs) {
					element.on('click','a',function(){
						var _this=$(this);
						if(!_this.next().hasClass('nav-second-level')){
							$('.sidebar-collapse').find('li').removeClass('on');
							_this.parent('li').addClass('on');
						}else{

						}
					})				
				}
			};
		});
		// app.config(function($stateProvider, $urlRouterProvider) {
		//     // $urlRouterProvider.otherwise('');
		//     $stateProvider
		//         .state('indexpage', {
		//             url: 'wechat/indexpage',
		//             templateUrl: '/wechat/indexpage'
		//         })
		//         .state('addcandidate', {
		//             url: '/addcandidate',
		//             templateUrl: '/wechat'
		//         })
		// });
	</script>
	<script>
		// $('#All_Candidate').click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "forHr/allcandidate"
		// 				});

		// $('#Interview_LifeCycle').parent().find('div').hide();
		// $('#Interview_LifeCycle').click(function() {
		// 	// 						
		// 	$(this).parent().find('div').slideToggle("fast");

		// });

		// $('#Q0').click(function() {
		// 	window.frames["iframe_info"].location.href = "forHr/roundmovenr"
		// });
		// $('#Q1')
		// 		.click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "forHr/roundmover"
		// 				});
		// $('#Q2')
		// 		.click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "forHr/roundmove1"
		// 				});
		// $('#Q3')
		// 		.click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "forHr/roundmove2"
		// 				});
		// $('#Q4')
		// 		.click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "forHr/roundmove3"
		// 				});
		// $('#Q5')
		// .click(
		// 		function() {
		// 			window.frames["iframe_info"].location.href = "forHr/roundmovep"
		// 		});
		// $('#Trash')
		// 		.click(
		// 				function() {
		// 					window.frames["iframe_info"].location.href = "www.baidu.com"
		// 				});
		// $('#addcandidate')
		// .click(
		// 		function() {
		// 			window.frames["iframe_info"].location.href = "http://localhost:8080/wechat/"
		// 		});
		// $('#help')
		// .click(
		// 		function() {
		// 			window.frames["iframe_info"].location.href = "www.baidu.com"
		// 		});
	</script>


	<style id="antiClickjack">
body {
	display: none !important;
}
</style>
	<script>
		if (self === top) {
			var antiClickjack = document.getElementById("antiClickjack");
			antiClickjack.parentNode.removeChild(antiClickjack);
		} else {
			top.location = self.location;
		}
	</script>
</body>
</html>