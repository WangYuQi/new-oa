<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css" href="resources/css/WHO_CSS.css">
<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<title>Intern Information</title>

<!-- Bootstrap core CSS -->
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<%=path %>/resources/css/starter-template.css" rel="stylesheet">
<link href="<%=path %>/resources/css/font.css" rel="stylesheet">
<script type="text/javascript" src="<%=path %>/resources/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top white">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar" id="nav">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
				&nbsp;&nbsp;&nbsp;<img src="<%=path %>/resources/images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#personalinformation"><b>Personal information</b></a></li>
					<li><a href="#jobinformation"><b>Position information</b></a></li>
					<li><a href="#statusinformation"><b>Status information</b></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">	
		<div class="page-header" style="text-align:center; margin: 10px 0 20px;">
			<h4>PwC SDC</h4>
			<h3><b>Intern Application Form</b></h3>
			
		</div>		
		<div class="list-group" id="personalinformation">
			<a href="##" class="list-group-item list-group-item-warning	"><b>Personal information</b></a>
				<div class="table-responsive">
					<table class="table table-condensed">
					  <tbody>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="chinesename">Chinese Name:</label></td>
								<td style="width: 30px"><input id="chinesename" autofocus="autofocus" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>	
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="englishname">English Name：</label></td>
								<td style="width: 150px"><input id="englishname" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Gender&nbsp;&nbsp;&nbsp;&nbsp;：</b></td>
								<td style="width: 150px; font-size: 12pt;"><input type="radio"  value="option1" name="sex" id="sex">male&nbsp;&nbsp;&nbsp;
									<input type="radio"  value="option2"  name="sex" id="sex">female</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="IDNumber">ID Number：</label></td>
								<td style="width: 150px"><input id="IDNumber" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="birthday">Date of birth：</label></td>
								<td style="width: 150px"><input id="birthday" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Education&nbsp;&nbsp;Background&nbsp;&nbsp;&nbsp;：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 155px;">
									  <option>college diploma</option> 
				                      <option>university diploma</option> 
				                      <option>master degree</option> 
				                      <option>doctor degree</option> 
				                      <option>post-doctor</option> 
									</select>
								</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="university">Undergraduate university：</label></td>
								<td style="width: 150px"><input id="university" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="graduate">Postgraduates university：</label></td>
								<td style="width: 150px"><input id="graduate" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Location of university：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 150px;">
									  <option>Shanghai</option> 
									  <option>Wuhan</option> 
									  <option>Dalian</option> 
									  <option>Overseas</option> 
									  <option>Other</option>
									</select>
								</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<div class="form-group">
									<td style="width: 50px;font-size: 12pt;"><b>Language skill：</b></td>
									<td style="width: 150px; font-size: 12pt;">			  
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option1">CET-4</label>	
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option2">CET-6</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option3">TEM-4</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option4">TEM-8</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option5">JPT-1</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option6">JPT-2</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option7">JPT-3</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option8"><label for="other">other:</label></label>
										<input id="other" style="width: 100px;border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</div>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="major">Major：</label></td>
								<td style="width: 150px"><input id="major" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="email">E-mail&nbsp;&nbsp;：</label></td>
								<td style="width: 150px"><input type="email" id="email" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff;font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="phone">Phone&nbsp;&nbsp;Number&nbsp;&nbsp;&nbsp;：</label></td>
								<td style="width: 150px"><input id="phone" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Veteran status：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 150px;">
									  <option>Already served</option> 
									  <option>Not served</option>
									</select>
								</td>
							</form>	
						</tr>				
					</tbody>	
				</table>
			</div>
		</div>
		<div class="list-group" id="jobinformation">
			<a href="##" class="list-group-item list-group-item-warning	"><b>Position information</b></a>
			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>	
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Position applied to：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 100px;">
									  <option>HR</option> 
									  <option>GW</option> 
									  <option>1</option> 
									  <option>2</option> 
									  <option>3</option> 
									</select>
								</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Graduation Date：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 100px;">
									  <option>2015</option> 
									  <option>2016</option> 
									  <option>2017</option> 
									</select>
								</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Attendance days：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 100px;">
									  <option>3</option> 
									  <option>4</option> 
									  <option>5</option>
									</select>
								</td>
							</form>	
						</tr>
						<tr>							
							<td style="width: 50px;font-size: 12pt;"><b>Date of availability：</b></td>
							<td style="width: 150px; font-size: 12pt;">	
								<form class="form-inline" role="form">																	  
								<select style="width: 65px;"  id="year">
								  <option>2016</option> 
								  <option>2017</option> 
								  <option>2018</option>
								</select>year								
								<select style="width: 45px;" id="month">
								  <option>1</option> 
								  <option>2</option> 
								  <option>3</option> 
								  <option>4</option> 
								  <option>5</option> 
								  <option>6</option>
								  <option>7</option> 
								  <option>8</option> 
								  <option>9</option>
								  <option>10</option> 
								  <option>11</option> 
								  <option>12</option>
								</select>month 
								</form>	
							</td>							
						</tr>						
					</tbody>	
				</table>
			</div>
		</div>	
		<div class="list-group" id="statusinformation">
		<a href="##" class="list-group-item list-group-item-warning	"><b>Status information</b></a>
			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>	
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="RoundScore1">Round score 1：</label></td>
								<td style="width: 150px"><input id="RoundScore1" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="RoundScore2">Round score 2：</label></td>
								<td style="width: 150px"><input id="RoundScore2" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px; font-size: 12pt;"><label
									for="RoundScore3">Round score 3：</label></td>
								<td style="width: 150px"><input id="RoundScore3"
									style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 120px; font-size: 12pt;"><b>Status：</b></td>
								<td style="width: 150px; font-size: 12pt;"><select
									class="form-control" style="width: 150px;">
										<option>not started</option>
										<option>Round 1</option>
										<option>Round 2</option>
										<option>Round 3</option>
										<option>Pass</option>
										<option>Fail</option>
								</select></td>
							</form>
						</tr>


					</tbody>	
				</table>
			</div>
		</div>					
		<div style="text-align:center;">
			<button class="btn btn-warning" type="button" style="padding: 6px 100px;" id="tijiao">Submit</button><br><br>
		</div>
		<div class="modal fade" style="vertical-align: middle; text-align: center;" id="confirm">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title">Please confirm your information then sign！</h4>
					</div>
					<form role="form" style="margin-bottom:0;" class="modal-tmp">
					<div class="modal-body">
					<div ng-app="">
					<h1 style="font-family: stxingkairegular;"> {{name}}</h1>						
						<input placeholder="请在此签字：" type="text" class="form-control" required="" autocomplete="on" value="" ng-model="name" autofocus="autofocus">
					</div>
					</div>
					</form>
					<div class="modal-footer">
						<a href="javascript:confirmSubmit()"><button type="button" class="btn btn-warning" data-dismiss="modal" id="sure">Sure</button></a>
						<button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>		
		<div class="modal fade" style="vertical-align: middle; text-align: center;" id="submit">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title">Submit</h4>
					</div>
					<div class="modal-body">
						<p>Submit Success！</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<!--<a href="javascript:confirmSubmit()"><button type="button" class="btn btn-warning">Sure</button></a>-->
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
	</div>
	<!-- /.container -->
		<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/js/jquery-1.11.3.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/angular.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
	<script>
	  $(function(){
		$("#sure").click(function(){
		  $("#submit").modal("toggle");
		  keyboard:true;

		});
	  });
	</script>
	<script>
	  $(function(){
		$("#tijiao").click(function(){
		  $("#confirm").modal("toggle");
		  keyboard:true;
		});
	  });
	</script>
</body>
</html>