<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<title>Intern Detail</title>

<!-- Bootstrap core CSS -->
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=path %>/resources/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<%=path %>/resources/css/starter-template.css"
	rel="stylesheet">
<script type="text/javascript"
	src="<%=path %>/resources/js/bootstrap.min.js"></script>

</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top white">

		<div class="container">

			<div class="navbar-header">

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
					&nbsp;&nbsp;&nbsp;<img
						src="<%=path %>/resources/images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">

					<li><a href="#personalinformation"><b>Personal
								information</b></a></li>
					<li><a href="#jobinformation"><b>Position information</b></a></li>
					<li><a href="#statusinformation"><b>Status information</b></a></li>
					<li><a href="internlist"><b>Return to list page</b></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">
		<div class="page-header"
			style="text-align: center; margin: 10px 0 20px;">
			<h3>Intern Application Form</h3>
			<h4>PwC SDC</h4>
		</div>
		<div id="personalinformation" style="text-align: center;">
			<h4>Personal information</h4>
		</div>
		<HR
			style="FILTER: alpha(opacity = 100, finishopacity = 0, style = 3); margin-top: 10px; height: 1;"
			width="80%" color=#fd6709 SIZE=3>
		<form role="form" style="font-size: 18px;">
			<div class="form-group">
				Chinese Name： <label class="radio-inline"> <input
					type="text" autofocus="autofocus" style="width: 150px;"
					class="form-control" id="chinesename"></label>
			</div>
			<div class="form-group">
				English Name： <label class="radio-inline"> <input
					type="text" style="width: 150px;" id="englishname"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Gender&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp; <label class="radio-inline">
					<input type="radio" value="option1" name="sex">male
				</label> <label class="radio-inline"> <input type="radio"
					value="option2" name="sex">female
				</label>
			</div>
			<div class="form-group">
				ID Number： <label class="radio-inline"> <input type="text"
					style="width: 300px;" id="IDNumber" class="form-control" /></label>
			</div>
			<div class="form-group">
				Date of birth： <label class="radio-inline"> <input
					type="text" style="width: 300px;" name="birthday"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Education&nbsp;&nbsp;Background&nbsp;&nbsp;： <label
					class="radio-inline"> <select class="form-control">
						<option>college diploma</option>
						<option>university diploma</option>
						<option>master degree</option>
						<option>doctor degree</option>
						<option>post-doctor</option>
				</select>
				</label>
			</div>
			<div class="form-group">
				Undergraduate university： <label class="radio-inline"> <input
					type="text" style="width: 200px;" name="university"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Postgraduates university： <label class="radio-inline"> <input
					type="text" style="width: 200px;" name="graduate"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Location of university： <label class="radio-inline"> <select
					class="form-control">
						<option>Shanghai</option>
						<option>Wuhan</option>
						<option>Dalian</option>
						<option>Overseas</option>
						<option>Other</option>
				</select>
				</label>
			</div>
			<div class="form-group">
				Language skill： <label class="checkbox-inline"> <input
					type="checkbox" value="option1">CET-4
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option2">CET-6
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">TEM-4
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">TEM-8
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">JPT-1
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">JPT-2
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">JPT-3
				</label> <label class="checkbox-inline"> <input type="checkbox"
					value="option3">Other<label class="radio-inline"> <input
						type="text" style="width: 200px;" name="other"
						class="form-control" /></label>
				</label>
			</div>
			<div class="form-group">
				Major： <label class="radio-inline"> <input type="text"
					style="width: 200px;" name="major" class="form-control" /></label>
			</div>
			<div class="form-group">
				E-mail&nbsp;&nbsp;： <label class="radio-inline"> <input
					type="email" style="width: 300px;" name="email"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Phone&nbsp;&nbsp;Number&nbsp;&nbsp;： <label class="radio-inline">
					<input type="email" style="width: 300px;" name="phone"
					class="form-control" />
				</label>
			</div>
			<div class="form-group">
				Veteran status： <label class="radio-inline"> <select
					class="form-control">
						<option>Already served</option>
						<option>Not served</option>
				</select>
				</label>
			</div>
		</form>

		<div id="statusinformation" style="text-align: center;">
			<h4>Status information</h4>
		</div>
		<HR
			style="FILTER: alpha(opacity = 100, finishopacity = 0, style = 3); margin-top: 10px; height: 1;"
			width="80%" color=#fd6709 SIZE=3>
		<form role="form" style="font-size: 18px;">
			<div class="form-group">
				Round 1 score: <label class="radio-inline"> <input
					type="text" autofocus="autofocus" style="width: 150px;"
					class="form-control" id="chinesename"></label>
			</div>
			<div class="form-group">
				Round 2 score: <label class="radio-inline"> <input
					type="text" style="width: 150px;" id="englishname"
					class="form-control" /></label>
			</div>
			<div class="form-group">
				Round 3 score: <label class="radio-inline"> <input
					type="text" style="width: 150px;" id="englishname"
					class="form-control" /></label>
			</div>

			<div class="form-group">
				Status： <label class="radio-inline"> <select
					class="form-control">
						<option>not started</option>
						<option>Round 1</option>
						<option>Round 2</option>
						<option>Round 3</option>
						<option>Pass</option>
						<option>Fail</option>
				</select>
				</label>
			</div>

		</form>

		<form role="form" style="font-size: 18px;">
			<div id="jobinformation" style="text-align: center;">
				<h4>Position information</h4>
			</div>
			<HR
				style="FILTER: alpha(opacity = 100, finishopacity = 0, style = 3); margin-top: 10px; height: 1;"
				width="80%" color=#fd6709 SIZE=3>
			<div class="form-group">
				Position applied to： <label class="radio-inline"> <select
					class="form-control">
						<option>HR</option>
						<option>GW</option>
						<option>1</option>
						<option>2</option>
						<option>3</option>
				</select>
				</label>
			</div>
			<div class="form-group">
				Graduation Date： <label class="radio-inline"> <select
					class="form-control">
						<option>2015</option>
						<option>2016</option>
						<option>2017</option>
				</select>
				</label>
			</div>
			<div class="form-group">
				Attendance days： <label class="radio-inline"> <select
					class="form-control">
						<option>3</option>
						<option>4</option>
						<option>5</option>
				</select>
				</label>
			</div>
			<div class="form-group">Date of availability：
		</form>
		<form class="form-horizontal" role="form" style="font-size: 18px;">
			<label class="radio-inline"> <select class="form-control">
					<option>2016</option>
					<option>2017</option>
					<option>2018</option>
			</select></label>year <label class="radio-inline"> <select
				class="form-control">
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>6</option>
					<option>7</option>
					<option>8</option>
					<option>9</option>
					<option>10</option>
					<option>11</option>
					<option>12</option>
			</select></label>month
	</div>
	</form>
	<!--<form action="" class="form-horizontal"  role="form">
        <fieldset>
			<div class="form-group">
                <label for="dtp_input2" class="col-md-2 control-label">Date Picking</label>
                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
				<input type="hidden" id="dtp_input2" value="" /><br/>
            </div>
        </fieldset>
    </form>-->
	<div style="text-align: center;">
		<button class="btn btn-warning" type="button"
			style="padding: 6px 100px;">Save</button>
		<br>
		<br>
	</div>
	</form>
	<div class="modal fade"
		style="vertical-align: middle; text-align: center;" id="mymodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title">Save</h4>
				</div>
				<div class="modal-body">
					<p>Are you sure to save?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a href="javascript:confirmSubmit()"><button type="button"
							class="btn btn-warning">Sure</button></a>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	</div>
	<!-- /.container -->
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/js/jquery-1.11.3.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/bootstrap-datetimepicker.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
	<script>
	  $(function(){
		$(".btn").click(function(){
		  $("#mymodal").modal("toggle");
		  keyboard:true;
		});
	  });
	</script>
	<script type="text/javascript">
	$('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		pickerPosition: "bottom-left"
    });
</script>
</body>
</html>