<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css"
	href="<%=path%>/resources/css/WHO_CSS.css">
<link href="<%=path%>/resources/css/starter-template.css"
	rel="stylesheet">
<link href="<%=path%>/resources/css/font.css" rel="stylesheet">
<link href="<%=path%>/resources/css/bootstrap.min.css" rel="stylesheet">
<link rel='stylesheet' type="text/css"
	href="<%=path%>/resources/css/inquireStyle.css" />
<script type="text/javascript" src="<%=path%>/resources/js/jquery-1.11.3.min.js" /></script>
<script type="text/javascript" src="<%=path%>/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/bootstrap-paginator.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/inquirePage.js"></script>
<title>list</title>



</head>

<body class="container" id="select">
	<div><h3>Please select the conditions</h3></div>
	<ul class="select">
		<li class="select-list">
			<dl id="select1">
				<dt>Selected position：</dt>
				<dd>　　　</dd>
				<dd class="select-all selected"><a href="#">Cancel</a></dd>
				<dd><a href="#" value="java">Java Intern</a></dd>
				<dd><a href="#">Guidwire Intern</a></dd>
				<dd><a href="#">HR Intern</a></dd>
				<dd><a href="#">PQA Intern</a></dd>
				<dd><a href="#">Admin Intern</a></dd>
				<dd><a href="#">Orcale Intern</a></dd>
			</dl>
		</li>
		<li class="select-list">
			<dl id="select2">
				<dt>Day of week：</dt>
				<dd>　　　</dd>
				<dd class="select-all selected"><a href="#">Cancel</a></dd>
				<dd><a href="#">One Day</a></dd>
				<dd><a href="#">Two Days</a></dd>
				<dd><a href="#">Three Days</a></dd>
				<dd><a href="#">Four Days</a></dd>
				<dd><a href="#">Five Days</a></dd>
			</dl>
		</li>
		<li class="select-list">
			<dl id="select3">
				<dt>Condition：</dt>
				<dd>　　　</dd>
				<dd class="select-all selected"><a href="#">Cancel</a></dd>
				<dd><a href="#">Yes</a></dd>
				<dd><a href="#">No</a></dd>
			</dl>
		</li>
		<li class="select-result">
			<dl>
				<dt>Selected condition：</dt>
				<dd>　　　　</dd>
			</dl>
		</li>
	</ul>
	<span id="xianshi"></span>
	<div class="container">
		<div>
			<button type="button" class="btn btn-warning"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Select All</button>
			<button type="button" class="btn btn-warning"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Round Move</button>
			<button type="button" class="btn btn-warning"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Report</button>
		</div>
		<table class="table table-hover" id="tab1">
			<thead>
				<tr>
					
					<th style="width: 50px">No</th>
					<th style="width: 20px">Name</th>
					<th style="width: 20px">Position</th>
					<th style="width: 50px">University</th>
					<th style="width: 20px">Major</th>
					<th style="width: 90px"><div class="row">
							<div class="col-md-4">Rs1</div>
							<div class="col-md-4">Rs2</div>
							<div class="col-md-4">Rs3</div>
						</div></th>
					<th style="width: 20px"></th>
					<th style="width: 20px">Status</th>
					<th style="width: 20px">Result</th>
					<th style="width: 20px"></th>
					<th style="width: 20px"></th>
				</tr>
			</thead>
			<tbody>
<%-- 				<c:forEach items="${candidateinfoall}" var="app" varStatus="sta"> --%>
<!-- 					<tr> -->
<!-- 						<td style="width: 10px"><input type="checkbox" value="" -->
<!-- 							id="checkbox"></td> -->
<!-- 						<td style="width: 10px"></td> -->
<%-- 						<td style="width: 20px">${app.name}</td> --%>
<%-- 						<td style="width: 20px">${app.applyPosition}</td> --%>
<%-- 						<td style="width: 50px">${app.university}</td> --%>
<%-- 						<td style="width: 20px">${app.major}</td> --%>
<!-- 						<td style="width: 90px"><div class="row"> -->
<%-- 								<div class="col-md-4">${app.round1Score}</div> --%>
<%-- 								<div class="col-md-4">${app.round2Score}</div> --%>
<%-- 								<div class="col-md-4">${app.round3Score}</div> --%>
<!-- 							</div></td> -->
<!-- 						<td style="width: 20px"></td> -->
<%-- 						<td style="width: 20px">${app.status}</td> --%>
<%-- 						<td style="width: 20px">${app.result}</td> --%>
<!-- 						<td style="width: 20px"><span -->
<!-- 							class="glyphicon glyphicon-trash"></span></td> -->
<!-- 						<td style="width: 20px"><span -->
<!-- 							class="glyphicon glyphicon-pencil"></span></td> -->
<%-- <%-- 						<td style="display: none">${app.nationalId}</td> --%> 
<%-- <%-- 						<td style="display: none">${app.applyId}</td> --%> 
<%-- <%-- 						<td style="display: none">${app.applyDate}</td> --%> 
<!-- 					</tr> -->
<%-- 				</c:forEach> --%>
			</tbody>
		</table>
		<div>
			<button type="button" class="btn btn-warning" id="btn1"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">The First Page</button>
			<button type="button" class="btn btn-warning" id="btn2"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Pre Page</button>
			<button type="button" class="btn btn-warning" id="btn3"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">Next Page</button>
			<button type="button" class="btn btn-warning" id="btn4"
				style="padding: 5px 12px;" data-toggle="modal"
				data-target="#myModal3">The last Page</button>
		</div>

		<!-- /.modal -->
		<div class="footerLogo"></div>
	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
