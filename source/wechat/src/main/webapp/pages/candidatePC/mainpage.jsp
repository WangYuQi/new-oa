<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html ng-app="CandidateModulePC">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/font.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/weui.min.css" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/roundmove.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/CandidatePC.css"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/importExcelDropify.min.css"/>"/>
<script>
   	var name = "<sec:authentication property="name" htmlEscape="false"/>"
</script>
<title>Job Application</title>
</head>
<body data-ng-controller="candidateController">
    <!-- top-nav -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand">
                <img src="/wechat/resources/images/logo.png">
                <span class="">School Recuitment</span>
            </a>
        </div>
    </div>
    <!-- side-nav -->
	<div ui-view='sideMenu'></div>
    <div ui-view='content' class="content"></div>
</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.nicescroll.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-1.3.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-ui-router.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prettify.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.js"/>"></script>
<script type="text/javascript" src="<%=path%>/resources/js/routerPC.js" charset="utf-8"></script>
<script type="text/javascript" src="/wechat/resources/js/candidate.js" charset="utf-8"></script>
<script type="text/javascript" src="/wechat/resources/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/wechat/resources/js/errorinfo.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dropify.min.js"/>"></script>

<script type="text/javascript">
		$(function () {
            $( '#menu' ).niceScroll({
            cursorcolor: '#ccc',
            railalign: 'right', 
            cursorborder: "none", 
            horizrailenabled: false, 
            zindex: 2001, 
            cursoropacitymax: 1,
            // autohidemode:'false',
            spacebarenabled: false 
        });
    		$(window).bind("resize", function() {
    		     if ($(this).width() < 768) {
    		        $('div.sidebar-collapse').addClass('collapse')
    		    } else {
    		        $('div.sidebar-collapse').removeClass('collapse')
    		    }
    		});
    		$('.navbar-toggle').on('click', function () {
    		    if ($('#menu.hidden-xs').length){
    		        $('#menu').removeClass('hidden-xs');
    		    }
    		    else{
    		        $('#menu').addClass('hidden-xs');
    		    }
    		});
  		});
  		var config = {
            params : {
                source : 'HRAddCandidate'
            }
        };
</script>
</html> 