<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Login</title>
</head>
<body>
	<form name="f" action="login" method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<fieldset>
			<c:if test="${param.error != null}">
				<p><span style="color:red;">Invalid username and password.</span></p>
			</c:if>
			<c:if test="${param.logout != null}">
				<p><span style="color:red;">You have been logged out.</span></p>
			</c:if>
			<legend>Please Login</legend>
			<label for="username">Username</label> <input type="text"
				id="username" name="username" /> <label for="password">Password</label>
			<input type="password" id="password" name="password" />
			<div class="form-actions">
				<button type="submit" class="btn">Log in</button>
			</div>
		</fieldset>
	</form>
</body>
</html>