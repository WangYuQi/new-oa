var app = angular.module('offerModule', ['ngAnimate', 'ui.bootstrap', 'ui.router', 'ui.tinymce', 'ui.bootstrap.contextMenu']);
app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('process', {
            url: '/',
            templateUrl: 'html/process.html'
        })
        .state('sheets', {
            url: '/sheets',
            templateUrl: 'forHr/reportlist',
            controller: 'report'
        })
        .state('newhr', {
            url: '/newhr',
            templateUrl: 'forHrm/newHr',
            controller: 'hrManager'
        })
        .state('help', {
            url: '/help',
            templateUrl: 'html/help.html'
        })
        .state('addcandidate', {
            url: '/addcandidate',
            templateUrl: '/wechat/html/candidate/candidate_hr.html',
        })
        .state('addcandidate.basic', {
            url: '/basic',
            templateUrl: '/wechat/html/candidate/basicInformation.html',
        })
        .state('addcandidate.education', {
            url: '/education',
            templateUrl: '/wechat/html/candidate/educationSkill.html',
        })
        .state('addcandidate.survey', {
            url: '/survey',
            templateUrl: '/wechat/html/candidate/survey.html',
        })
        .state('addcandidate.preview', {
            url: '/preivew',
            templateUrl: '/wechat/html/candidate/preview.html',
        })
});
