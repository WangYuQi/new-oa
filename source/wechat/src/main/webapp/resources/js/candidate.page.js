var app = angular.module('candidatePage', [ 'ngCookies' ]);
app.controller('candidateController', function($cookies, $scope, $http, $interval) {
			$scope.LanguageSkillinvalid = true;
			$scope.eduExpinvalid = true;
			$scope.LanguageSkillError = true;
			$scope.eduExpError = true;
			// $scope.candidate.graduatePlan = "Work";
			$scope.changelanguagelevel = function(){
				var flage_skill=0;

				for(var i=0;i<=6;i++){
					if($scope.languageLevel[i] != null) flage_skill=1;
				}

				if($scope.languageLevel[7]!='' && $scope.languageLevel[7]){
					flage_skill=1;
				}
				if(flage_skill==1){
					$scope.LanguageSkillinvalid = false;
					
					// $("#LanguageSkillError").hide();
					$scope.LanguageSkillError = false;
				}
				else{
					$scope.LanguageSkillinvalid = true;
					
					// $("#LanguageSkillError").show();
					$scope.LanguageSkillError = true;
				}
			}
			// $scope.changeSkillOther = function($scope){
			// 	var flage_skill=0;
			// 	for(var i=0;i<$scope.languageLevel.length;i++){
			// 		if($scope.languageLevel[i] != null) flage_skill=1;
			// 	}
			// 	if($scope.languageLevel[$scope.languageLevel.length]!=''){
			// 		flage_skill=1;
			// 	}
			// 	if(flage_skill==1){
			// 		$scope.LanguageSkillinvalid = false;
			// 		$scope.$apply();
			// 		$("#LanguageSkillError").hide();
			// 	}
			// 	else{
			// 		$scope.LanguageSkillinvalid = true;
			// 		$scope.$apply();
			// 		$("#LanguageSkillError").show();
			// 	}
			// }

			$scope.GraduatePlanchange = function(){
				if($scope.candidate.graduatePlan == "Other"){
					$scope.graduatePlanother = true;
				}else{
					$scope.graduatePlanother = false;
				}
			}
			$scope.onChangeOther = function(){
				$scope.graduatePlan_tmp2 = 'Other-' + $scope.graduatePlan_tmp;
			}
			$scope.submitCandidate = function() {
				$scope.signatureDialog.show = false;
				var flage_skill=0;
				for(var i=0;i<$scope.languageLevel.length;i++){
					if($scope.languageLevel[i] != null) flage_skill=1;
				}
				if(flage_skill==0 ) {
					// $(".weui_check").focus();
					return;
				}
				$scope.loadingToast.show = true;
				if($scope.graduatePlanother) $scope.candidate.graduatePlan = $scope.graduatePlan_tmp2
				var candidateInfo = $scope.candidate;
				candidateInfo.languageLevel = $scope.languageLevel.join(",");
				// console.log(candidateInfo);
				var config = {
					params : {
						positon : $scope.candidate.applicationPosition,
						source : 'WeiXin'
					}
				};
				$http.post('sendInfo', candidateInfo, config)
						.success(function(data) {
							$scope.loadingToast.show = false;
							$scope.successToast.show = true;
							window.location.href = "/wechat/forCandidate/showQuestionnaire?nationalId=" + $scope.candidate.nationalId;
						}).error(function(data) {
							$scope.loadingToast.show = false;
							$scope.notice.show = true;
						});
				 
			};
			$scope.addEduExp = function() {
				$scope.eduExpError = false;
				$scope.eduExpinvalid = false;
				$scope.candidate.educationExperiences.push({});
			};
			$scope.dropEduExp = function() {
				$scope.candidate.educationExperiences.pop();
				if($scope.candidate.educationExperiences.length == 0){
					$scope.eduExpinvalid = true;
					$scope.eduExpError = true;
				}
			};
			$scope.addWorkExp = function() {
				$scope.candidate.workExperiences.push({});
			};
			$scope.dropWorkExp = function() {
				$scope.candidate.workExperiences.pop();
			};
			$scope.showDialog = function() {
				$scope.signatureDialog.show = true;
			};
			$scope.hideDialog = function() {
				$scope.signatureDialog.show = false;
			};
			$scope.hideNotice = function() {
				$scope.notice.show = false;
			}
			$scope.$watch('candidate',function(){
				var jsonstr = JSON.stringify($scope.candidate);
				$cookies.put('$scope.candidate', jsonstr);
			},true)
			var content = $cookies.get('$scope.candidate');
			if (content == undefined) {
//				var config = {
//						params : {
//							wechatUid : 3084226
//						}
//				};
//				$http.post('forCandidate/searchWechatUid', '', config).success(
//				function(jsonCandidate) {
//					if (jsonCandidate != 'null') {
//						parseCandidate(jsonCandidate);
//					}
//				});
			} else {
//				var candidateInCookies = JSON.parse(content);
//				parseCandidate(candidateInCookies);
			}
			
			
			function parseCandidate(candidateObject) {
				$scope.candidate = candidateObject
				$scope.candidate.birthday = new Date(candidateObject.birthday);
				$scope.languageLevel = candidateObject.languageLevel.split(",");
				if($scope.candidate.graduatePlan.match("Other")){
					$scope.graduatePlanother = true;
					$scope.graduatePlan_tmp = $scope.candidate.graduatePlan.substring(6);
					$scope.candidate.graduatePlan = "Other";
				}else{
					$scope.graduatePlanother = false;
				}
			}
		});
app.directive('validateblur', function() {
	return {
		require : 'ngModel',
		restrict : 'A',
		link : function($scope, iElm, iAttrs, controller) {
			iElm.on('blur', function() {
				$scope.$apply(function() {
					controller.$blur = true;
				});
			});
		}
	};
});
