var myModule = angular.module("questionnaire", []);

myModule.controller('questionnaireController', function($scope, $http, $location) {
	
	$scope.languages=["english","中文"];	
	$scope.selectedLanguage = "english";
	getQuestions("english");
	
	
	function getQuestions(language){
		if(language!="chinese"&&language!="english")
			console("wrong language param");
		else {
		$http.get('getQuestions?language='+language)
		.success(function(data, status, headers, config){
	    $scope.questionnaires = data;
	    console.log(data)
	    }).error(function(data, status, headers, config){
	    console.log("get questions failed...");
	    });
		}
	}	
	
	function GetUrlParam(name)
	{
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	$scope.changeLanguage = function() {
		if ($scope.selectedLanguage == "中文")
		   getQuestions("chinese");
		else if($scope.selectedLanguage =="english")
		   getQuestions("english");
		else
		   console("wrong language param");
	}
	
	$scope.hideNotice = function(){
		$scope.dialogShow = false;
	}
	
	$scope.sendQuestionnaire = function(){
		
		var questionnaire = $scope.questionnaires;
		questionnaire.answer = GetUrlParam("nationalId");
		$http.post('insertQuestionnaire',questionnaire)
		.success(function(data) {
	    $scope.dialogShow=true;
		 console.log("send success...")
		}).error(function(data) {
         console.log("send failed...")
		});
	}
});


