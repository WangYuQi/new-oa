package com.pwc.wechat.entity.user;

public class UserRole {
	
	private String userName;
	private boolean hrFlag;
	private boolean hrmFlag;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean getHrFlag() {
		return hrFlag;
	}

	public void setHrFlag(boolean hrFlag) {
		this.hrFlag = hrFlag;
	}

	public boolean getHrmFlag() {
		return hrmFlag;
	}

	public void setHrmFlag(boolean hrmFlag) {
		this.hrmFlag = hrmFlag;
	}

}
