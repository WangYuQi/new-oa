package com.pwc.wechat.service.export.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.util.DateUtil;

public abstract class AbstractToExcel<T> implements IEntityWriter<T> {
	
	abstract void generateExcel (XSSFWorkbook workbook, T entity, int index);

	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, T entity) throws Exception {
		XSSFWorkbook workbook = this.initWorkbook();
		this.generateExcel(workbook, entity, BaseConst.EXCEL_START_ROW);
		workbook.write(outputStream);
		workbook.close();
	}

	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, List<T> entityList) throws Exception {
		XSSFWorkbook workbook = this.initWorkbook();
		for (int i = 0; i < entityList.size(); i++) {
			this.generateExcel(workbook, entityList.get(i), i + BaseConst.EXCEL_START_ROW);
		}
		workbook.write(outputStream);
		workbook.close();
	}
	
	@Override
	public void initResponse(HttpServletResponse response) {
		response.setHeader("Content-disposition", "attachment; filename=TalentDataSet" + DateUtil.getEpochSecond() + this.suffixFileName());
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	}
	
	@Override
	public String suffixFileName() {
		return ".xlsx";
	}
	
	private XSSFWorkbook initWorkbook() throws IOException {
		Resource excelFormat = new ClassPathResource("/document/ExcelFormat.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(excelFormat.getInputStream());
		XSSFCellStyle style = workbook.getCellStyleAt(0);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN); // 下边框
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);// 左边框
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);// 上边框
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);// 右边框
		return workbook;
	}
	
}
