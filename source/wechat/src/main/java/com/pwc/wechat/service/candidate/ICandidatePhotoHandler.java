package com.pwc.wechat.service.candidate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.candidate.CandidatePhoto;


	public interface ICandidatePhotoHandler {
		
		@Transactional(readOnly = false, rollbackFor = Exception.class)
		public void saveCandidatePhoto(CandidatePhoto candidatePhoto);
		
		@Transactional(readOnly = true, rollbackFor = Exception.class)
		public String createPhoto(String nationalId, HttpServletResponse response);
		
	}


