package com.pwc.wechat.service.export.writer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.pwc.wechat.util.DateUtil;

public abstract class AbstractToPdf<T> implements IEntityWriter<T> {
	
	private Resource yaHeiFontResource;
	private Font yaHeiFont;
	
	public AbstractToPdf() throws DocumentException, IOException {
		yaHeiFontResource = new ClassPathResource("/font/msyh.ttf");
		yaHeiFont = new Font(BaseFont.createFont(yaHeiFontResource.getFile().getAbsolutePath(), BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED), 12, Font.NORMAL);
	}
	
	abstract protected void generatePdf(Document pdf, T entity) throws DocumentException;
	
	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, T entity) throws Exception {
		ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
		Document pdf = new Document(PageSize.A4);
		this.initialPdfInfo(pdf);
		PdfWriter.getInstance(pdf, byteOutput);
		pdf.open();
		this.generatePdf(pdf, entity);
		pdf.close();
		byteOutput.close();
		outputStream.write(byteOutput.toByteArray());
	}
	
	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, List<T> entity) throws  Exception {
		Document pdf = new Document(PageSize.A4);
		this.initialPdfInfo(pdf);
		PdfWriter.getInstance(pdf, outputStream);
		pdf.open();
		for (T one : entity) {
			this.generatePdf(pdf, one);
		}
		pdf.close();
	}
	
	@Override
	public void initResponse(HttpServletResponse response) {
		response.setHeader("Content-disposition", "attachment; filename=TalentDataSet" + DateUtil.getEpochSecond() + this.suffixFileName());
		response.setContentType("application/pdf");
	}
	
	@Override
	public String suffixFileName() {
		return ".pdf";
	}
	
	protected void addParagraph(Document pdfDocument, String line) throws DocumentException {
		pdfDocument.add(new Paragraph(line, yaHeiFont));
	}
	
	private void initialPdfInfo(Document pdfDocument){
		pdfDocument.addTitle("Resume for PwC");
		pdfDocument.addSubject("Curriculum Vitae PDF");
		pdfDocument.addAuthor("Pwc SDC Shanghai");
		pdfDocument.addCreator("Pwc SDC Shanghai");
		pdfDocument.addProducer();
		pdfDocument.addCreationDate();
	}
	

}
