package com.pwc.wechat.util;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

import org.springframework.context.ApplicationContext;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.web.context.ContextLoader;
/**
 * 获取java bean的工具类
 * @author ochen033
 *
 */
public class ApplicationContextUtil {
	
	private ApplicationContextUtil() {
		
	}
	
	private static ApplicationContext applicationContext = ContextLoader.getCurrentWebApplicationContext();
	
	public static <T> T getBeanByClass(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getBeanByName(String name) {
		return (T) applicationContext.getBean(name);
	}
	
	public static EntityManager creatEntityManager() {
		return applicationContext.getBean(LocalContainerEntityManagerFactoryBean.class).getNativeEntityManagerFactory().createEntityManager();
	}
	public static CriteriaBuilder getCriteriaBuilder() {
		return applicationContext.getBean(LocalContainerEntityManagerFactoryBean.class).getNativeEntityManagerFactory().getCriteriaBuilder();
	}
	
}
