package com.pwc.wechat.entity.candidate;

public class CandidateAllInfo {

	private Candidate candidate;
	private Application application;
	private QuestAndAnswer questAndAnswer;

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public QuestAndAnswer getQuestAndAnswer() {
		return questAndAnswer;
	}

	public void setQuestAndAnswer(QuestAndAnswer questAndAnswer) {
		this.questAndAnswer = questAndAnswer;
	}

}
