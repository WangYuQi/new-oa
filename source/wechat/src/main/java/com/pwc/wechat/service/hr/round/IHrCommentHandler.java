package com.pwc.wechat.service.hr.round;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.hr.HrComment;

public interface IHrCommentHandler {

	@Transactional(readOnly = true)
	public HrComment searchHrComment(String nationalId);

	@Transactional(readOnly = false)
	public void updateHrComment(HrComment hrComment);

}
