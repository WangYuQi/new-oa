package com.pwc.wechat.entity.candidate;

import java.util.ArrayList;
import java.util.List;

public class QuestAndAnswer {

	private String question;// may be titile

	private String answer;// may be nationalId

	private List<QuestAndAnswer> questAndAnswerList = new ArrayList<QuestAndAnswer>();// key for question value for answer

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public List<QuestAndAnswer> getQuestAndAnswerList() {
		return questAndAnswerList;
	}

}
