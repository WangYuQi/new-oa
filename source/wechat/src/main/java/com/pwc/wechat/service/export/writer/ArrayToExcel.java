package com.pwc.wechat.service.export.writer;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

@Service("arrayToExcel")
public class ArrayToExcel extends AbstractToExcel<String[]> {

	@Override
	void generateExcel(XSSFWorkbook workbook, String[] entity, int index) {
		XSSFSheet sheet = workbook.getSheetAt(0);
		XSSFRow row = sheet.createRow(index);
		for (int i = 0; i < entity.length; i++) {
			XSSFCell cell = row.createCell(i);
			cell.setCellValue(entity[i]);
		}
	}

}
