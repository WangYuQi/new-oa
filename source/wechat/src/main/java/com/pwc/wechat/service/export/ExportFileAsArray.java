package com.pwc.wechat.service.export;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.service.export.writer.IEntityWriter;
import com.pwc.wechat.util.CommonUtil;

@Service("exportFileAsArray")
public class ExportFileAsArray extends AbstractExportFile<String[]> {
	
	@Autowired
	private HybridDao hybridDao;
	
	@Autowired
	private IEntityWriter<String[]> arrayToExcel;

	@Override
	protected IEntityWriter<String[]> initWriter() {
		return arrayToExcel;
	}

	@Override
	protected List<String[]> queryEntityList(String[] nationalIds) {
		ReportColumn[] columns = BaseConst.EXCEL_COLUMNS;
		String[] conditions = { this.inCollection(ReportColumn.CANDIDATE_NATIONAL_ID.getColumnData(), nationalIds) };
		return hybridDao.querySqlWithJpa(columns, conditions, (sqlResult, rowNum) -> {
			String[] res = new String[columns.length];
			res[0] = String.valueOf(sqlResult[0]).replace("-", "") + "-" + CommonUtil.format4Digits(Integer.valueOf(String.valueOf(sqlResult[1])));// format as 20150101-0001 
			for (int j = 1; j < columns.length-1; j++) { // start from 1 because 0 is for NO // columns.length-1 because 2 columns merge to 1 column as NO
				res[j] = String.valueOf(sqlResult[j + 1]); // start from 3 because ResultSet[1] is 2015-01-01 and ResultSet[2] is 0001
			}
			return res;
		});
	}

	@Override
	protected String[] searchByNationalId(String nationalId) {
		// search EntityList by nationalIds
		// don't need this method
		return null;
	}

	private String inCollection(String colName, String[] nationalIds) {
		StringBuilder collection = new StringBuilder();
		collection.append(colName);
		collection.append(" in (");
		Arrays.stream(nationalIds).forEach((String nationalId) ->{
			if (nationalId != null && !nationalId.isEmpty()) {
				collection.append("'" + nationalId + "',");
			}
		});
		if (collection.lastIndexOf(",") != -1) {
			return collection.substring(0, collection.lastIndexOf(",")) + ")";
		} else {
			return "";
		}
	}

}
