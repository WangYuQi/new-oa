package com.pwc.wechat.dao.candidate;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pwc.wechat.entity.candidate.CandidatePhoto;

	public interface ICandidatePhotoCrud extends JpaRepository<CandidatePhoto, String>{

	}

