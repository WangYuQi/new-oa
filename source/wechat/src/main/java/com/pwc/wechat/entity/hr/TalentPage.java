package com.pwc.wechat.entity.hr;

public class TalentPage {

	private int dataIndex = 0;
	private int dataTotal = 0;
	private int pageIndex = 1;
	private int pageTotal = 1;
	private int pageLines = 20;

	public int getDataTotal() {
		return dataTotal;
	}

	public void setDataTotal(int dataTotal) {
		this.dataTotal = dataTotal;
		this.pageTotal = dataTotal > this.pageLines ? dataTotal / this.pageLines + 1 : 1;
		this.pageIndex = 0 < this.pageIndex && this.pageIndex <= this.pageTotal ? this.pageIndex : 1;
		this.dataIndex = (this.pageIndex - 1) * this.pageLines;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public int getDataIndex() {
		return dataIndex;
	}

	public void setDataIndex(int dataIndex) {
		this.dataIndex = dataIndex;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageLines() {
		return pageLines;
	}

	public void setPageLines(int pageLines) {
		this.pageLines = pageLines;
	}

}
