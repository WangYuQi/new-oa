package com.pwc.wechat.controller.hr;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.entity.user.UserRole;
import com.pwc.wechat.service.hr.user.IUserHandler;
import com.pwc.wechat.service.hr.user.IUserRoleHandler;

@RestController
@RequestMapping(value = "/forHrm")
public class ForHrmController {

	@Autowired
	private IUserRoleHandler iUserRoleHandler;
	
	@Autowired
	private IUserHandler iUserHandler;

	private static final Logger logger = LogManager.getLogger(ForHrmController.class);

	@RequestMapping(value = { "/newHr" }, produces = "text/html; charset=utf-8")
	public ModelAndView newHr(HttpServletRequest request) {
		logger.info("HR Manager Welcome");
		return new ModelAndView("hrm/hrmAuthorization");
	}

	@RequestMapping(value = "/addNewUser", produces = "application/json; charset=utf-8")
	public List<UserRole> addNewUser(@RequestBody User user) {
		iUserHandler.insertUser(user);
		return this.getAuthority();
	}

	@RequestMapping(value = "/updateAuthority", produces = "application/json; charset=utf-8")
	public List<UserRole> updateAuth(@RequestBody List<UserRole> userRoles) {
		userRoles.forEach((UserRole userRole) -> {
			iUserRoleHandler.updateUserRole(userRole);
		});
		return this.getAuthority();
	}

	@RequestMapping(value = "/getUser", produces = "application/json; charset=utf-8")
	public List<UserRole> getAuthority() {
		return iUserRoleHandler.searchUserRoles();
	}

}
