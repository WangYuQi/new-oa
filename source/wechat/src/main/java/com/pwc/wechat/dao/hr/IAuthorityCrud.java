package com.pwc.wechat.dao.hr;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.user.Authority;

public interface IAuthorityCrud extends JpaRepository<Authority, Integer> {
	
	public Authority findOneByUserNameAndUserAuth(String userName, String userAuth);
	
}
