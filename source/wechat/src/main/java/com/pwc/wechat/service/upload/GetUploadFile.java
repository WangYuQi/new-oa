package com.pwc.wechat.service.upload;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartResolver;

@Service("getUploadFile")
public class GetUploadFile<T> implements IGetUploadFile<T> {
	
	@Autowired
	private MultipartResolver multipartResolver;

	@Override
	public List<T> getUploadFile(HttpServletRequest request, IMultipartFileHandler<T> handler) {
		List<T> uploadFiles = new ArrayList<>();
		multipartResolver.resolveMultipart(request).getFileMap().values().stream()
			.filter(file -> !file.getOriginalFilename().isEmpty())
			.forEach(thisFile -> uploadFiles.add(handler.handle(thisFile)));
		return uploadFiles;
	}

}
