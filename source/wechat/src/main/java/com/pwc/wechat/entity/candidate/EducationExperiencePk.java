package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

public class EducationExperiencePk implements Serializable {

	private static final long serialVersionUID = -8892701110789149291L;

	private String nationalId;
	private Integer experienceId;

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public Integer getExperienceId() {
		return experienceId;
	}

	public void setExperienceId(Integer experienceId) {
		this.experienceId = experienceId;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EducationExperiencePk) {
			return ((EducationExperiencePk)obj).nationalId.equals(this.nationalId) && ((EducationExperiencePk)obj).experienceId.equals(this.experienceId) ? true : false;
		}
		return false;
	}

}
