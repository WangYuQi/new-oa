package com.pwc.wechat.service.export.writer;

import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

public interface IEntityWriter<T> {

	/**
	 * turn entity into a doc file or a pdf file
	 * write the entity into the outputStream
	 * 
	 * @param outputStream
	 * @param entity
	 * @throws Exception
	 */
	public <O extends OutputStream> void writeEntity(O outputStream, T entity) throws  Exception;
	
	/**
	 * turn entity list into a doc file or a pdf file
	 * write the entity list into the outputStream
	 * 
	 * @param outputStream
	 * @param entity
	 * @throws Exception
	 */
	public <O extends OutputStream> void writeEntity(O outputStream, List<T> entity) throws  Exception;
	
	/**
	 * initial the file type and application for the response
	 * @param response
	 */
	public void initResponse(HttpServletResponse response);
	
	/**
	 * initial the name of file in a ZIP folder
	 * @param entity
	 * @return
	 */
	public String suffixFileName();
	
}
