package com.pwc.wechat.service.export.writer;

import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public abstract class AbstractToPrettyDocx<T> extends AbstractToDocx<T>{
	
	@Override
	protected XWPFDocument initDocument() throws IOException {
		Resource excelFormat = new ClassPathResource("/document/PrettyDocxFormat.docx");
		XWPFDocument document = new XWPFDocument(excelFormat.getInputStream());
		return document;
	}

}
