package com.pwc.wechat.dao.candidate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.pwc.wechat.entity.candidate.Candidate;

public interface CandidatePagingAndSorting extends PagingAndSortingRepository<Candidate, String>{
	
	public Page<Candidate> findAllByGenderAndWechatUid(String gender, String wechatUid, Pageable pageable);

}
