package com.pwc.wechat.service.upload;

import org.springframework.web.multipart.MultipartFile;

@FunctionalInterface
public interface IMultipartFileHandler<T> {
	
	public T handle(MultipartFile file);

}
