package com.pwc.wechat.dao.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.EducationExperiencePk;

public interface IEducationExpCrud extends JpaRepository<EducationExperience, EducationExperiencePk>{
	
	public List<EducationExperience> findAllByNationalId(String nationalId);
	
	public void deleteAllByNationalId(String nationalId);

}
