package com.pwc.wechat.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.Random;

import javax.servlet.MultipartConfigElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

public class CommonUtil {
	
	private static final Logger logger = LogManager.getLogger(CommonUtil.class);
	
	private static Random random = new Random();
	private static NumberFormat format2Digits = NumberFormat.getInstance();
	private static NumberFormat format4Digits = NumberFormat.getInstance();
	private static Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
	
    public static String format2Digits(int num) {
    	format2Digits.setGroupingUsed(false);  
		format2Digits.setMinimumIntegerDigits(2);
    	return format2Digits.format(num);
	}
    
    public static String format4Digits(int num) {
    	format4Digits.setGroupingUsed(false);  
		format4Digits.setMinimumIntegerDigits(4);
    	return format4Digits.format(num);
	}
    
    public static Random random() {
		return random;
	}
    
    public static String encodePassword(String password) {
    	return md5PasswordEncoder.encodePassword(password, null);
	}
    
    public static MultipartConfigElement genUploadConfig(String path) {
    	Path tempUploadPath = Paths.get(path);
		if (Files.notExists(tempUploadPath)) {
			try {
				Files.createDirectory(tempUploadPath);
			} catch (IOException e) {
				logger.info("Create folder failed {}", tempUploadPath.toString(), e);
			}
		}
		return new MultipartConfigElement(tempUploadPath.toString(), 8000000L, 8000000L, 0);//8Mb
	}
   
}