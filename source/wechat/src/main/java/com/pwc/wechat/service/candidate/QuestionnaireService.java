package com.pwc.wechat.service.candidate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.IQuestionCrud;
import com.pwc.wechat.dao.candidate.IQuestionnaireCrud;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.entity.candidate.Questionnaire;
import com.pwc.wechat.entity.candidate.Questions;
import com.pwc.wechat.util.JsonUtil;

@Service("questionnaireService")
public class QuestionnaireService implements IQuestionnaireHandler{
	
	private Map<String, List<String>> questions = new HashMap<>();
	
	@Autowired
	private IQuestionnaireCrud iQuestionnaireCrud;
	
	@Autowired
	private IQuestionCrud iQuestionCrud;
	
	@Override
	public void saveQuestAndAnswer(QuestAndAnswer questAndAnswer) {
		Questionnaire questionnaire = new Questionnaire(); 
		questionnaire.setNationalId(questAndAnswer.getAnswer());// outside answer as nationalId
		questionnaire.setContent(JsonUtil.toNormalJson(questAndAnswer));
		iQuestionnaireCrud.save(questionnaire);
	}

	@Override
	public QuestAndAnswer searchQuestAndAnswer(String language) {
		QuestAndAnswer questAndAnswer = new QuestAndAnswer();
		if (this.questions.keySet().isEmpty()) {
			this.refreshQuestions();
		}
		this.questions.get(language).forEach(one->{
			QuestAndAnswer quest = new QuestAndAnswer();
			quest.setQuestion(one);
			questAndAnswer.getQuestAndAnswerList().add(quest);
		});
		return questAndAnswer;
	}
	
	private void refreshQuestions() {
		questions.clear();
		List<String> englishQuestions = new ArrayList<String>();
		List<String> chineseQuestions = new ArrayList<String>();
		iQuestionCrud.findAll().forEach((Questions question)->{
			if (question.getId() > 100) {
				chineseQuestions.add(question.getQuestion());
			} else {
				englishQuestions.add(question.getQuestion());
			}
		});
		questions.put("english", englishQuestions);
		questions.put("chinese", chineseQuestions);
	}
	
}
