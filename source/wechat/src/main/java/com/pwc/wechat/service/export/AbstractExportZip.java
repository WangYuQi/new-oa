package com.pwc.wechat.service.export;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import com.pwc.wechat.util.DateUtil;

public abstract class AbstractExportZip<T> extends AbstractExport<T, ZipOutputStream> {
	
	abstract protected String getFileName(T entity);
	
	@Override
	protected void initResponse(HttpServletResponse response) {
		response.setContentType("application/x-zip-compressed; charset=utf-8");
		response.setHeader("Content-Disposition", "attachment; filename=TalentZip" + DateUtil.getEpochSecond() + ".zip");// ZIP file name
	}

	@Override
	protected ZipOutputStream getOutputStream(HttpServletResponse response) throws IOException {
		return new ZipOutputStream(response.getOutputStream());
	}
	
	@Override
	protected void writeOne(ZipOutputStream outputStream, T entity) throws Exception {
		ZipEntry zipEntry = new ZipEntry(DateUtil.getLocalDate() + "/" + this.getFileName(entity) + this.initWriter().suffixFileName());//the directory inside the ZIP file
		outputStream.putNextEntry(zipEntry);
		this.initWriter().writeEntity(outputStream, entity);
		outputStream.closeEntry();
	}
	
}
