package com.pwc.wechat.entity.hr;

import java.util.List;

public class TalentDataSet {

	private List<TalentData> talentDatas;
	private TalentPage talentPage;

	public TalentPage getTalentPage() {
		return talentPage;
	}

	public void setTalentPage(TalentPage talentPage) {
		this.talentPage = talentPage;
	}

	public List<TalentData> getTalentDatas() {
		return talentDatas;
	}

	public void setTalentDatas(List<TalentData> talentDatas) {
		this.talentDatas = talentDatas;
	}

}
