package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "questionnaire", schema = "wechat")
public class Questionnaire implements Serializable {

	private static final long serialVersionUID = -5216438201387044595L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "national_id")
	private String nationalId;

	@Column(name = "content")
	private String content;// json of QuestAndAnswer

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	/**
	 * json of QuestAndAnswer
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * json of QuestAndAnswer
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
