package com.pwc.wechat.service.export.writer;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;

@Service("questAndAnswerToPdf")
public class QuestAndAnswerToPdf extends AbstractToPdf<QuestAndAnswer> {

	public QuestAndAnswerToPdf() throws DocumentException, IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void generatePdf(Document pdf, QuestAndAnswer entity) throws DocumentException {
		for (QuestAndAnswer one : entity.getQuestAndAnswerList()) {
			this.addParagraph(pdf, " ");
			this.addParagraph(pdf, one.getQuestion());
			this.addParagraph(pdf, one.getAnswer());
		}
	}

}
