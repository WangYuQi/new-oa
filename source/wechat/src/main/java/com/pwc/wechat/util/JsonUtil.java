package com.pwc.wechat.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 简单封装Jackson，实现JSON String<->Java Object的Mapper.
 * 
 * 封装不同的输出风格, 使用不同的builder函数创建实例.
 * 
 */
public class JsonUtil {
	
	private JsonUtil() {
		
	}

	private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);

	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public static JsonNode readAsJsonNode(String json) {
		try {
			return objectMapper.readTree(json.getBytes());
		} catch (Exception e) {
			logger.info("ReadTree Failed", e);
		}
		return null;
	}
	
	/**
	 * 输出全部属性
	 * 失败返回""
	 * @param object
	 * @return
	 */
	public static String toNormalJson(Object object) {
		return toJson(Include.ALWAYS, object);
	}

	/**
	 * 输出非空属性
	 * 失败返回""
	 * @param object
	 * @return
	 */
	public static String toNonNullJson(Object object) {
		return toJson(Include.NON_NULL, object);
	}

	/**
	 * 输出非Null且非Empty(如List.isEmpty)的属性
	 * 失败返回""
	 * @param object
	 * @return
	 */
	public static String toNonEmptyJson(Object object) {
		return toJson(Include.NON_EMPTY, object);
	}
	
	/**
	 * 转成Json
	 * @param include
	 * @param object
	 * @return
	 */
	private static String toJson(Include include, Object object) {
		try {
			objectMapper.setSerializationInclusion(include);
			return objectMapper.writeValueAsString(object);
		} catch (Exception e) {
			logger.info("Obj转Json报错：", e);
		}
		return null;
	}
	
	/**
	 * 输出全部属性
	 * 如果json中存在，对象中没有，则自动忽略该属性
	 * 失败返回null
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T toNormalObject(String json, Class<T> clazz) {
		return toObject(json, clazz);
	}
	
	/**
	 * 转成Object
	 * @param include
	 * @param json
	 * @param clazz
	 * @return
	 */
	private static <T> T toObject(String json, Class<T> clazz) {
		try {
			objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return objectMapper.readValue(json, clazz);
		} catch (Exception e) {
			logger.info("Json转Obj报错：", e);
		}
		return null;
	}

}
