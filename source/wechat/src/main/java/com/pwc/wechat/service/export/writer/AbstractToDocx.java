package com.pwc.wechat.service.export.writer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.pwc.wechat.util.DateUtil;

public abstract class AbstractToDocx<T> implements IEntityWriter<T> {
	
	abstract protected void generateDocx (XWPFDocument workbook, T entity, int index);

	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, T entity) throws Exception {
		ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
		XWPFDocument document = this.initDocument();
		this.generateDocx(document, entity, 0);
		document.write(byteOutput);
		document.close();
		byteOutput.close();
		outputStream.write(byteOutput.toByteArray());
	}

	@Override
	public <O extends OutputStream> void writeEntity(O outputStream, List<T> entity) throws Exception {
		XWPFDocument document = this.initDocument();
		for (int i = 0; i < entity.size(); i++) {
			this.generateDocx(document, entity.get(i), i);
		}
		document.write(outputStream);
		document.close();
	}

	@Override
	public void initResponse(HttpServletResponse response) {
		response.setHeader("Content-disposition", "attachment; filename=TalentDataSet" + DateUtil.getEpochSecond() + this.suffixFileName());
		response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
	}
	
	@Override
	public String suffixFileName() {
		return ".docx";
	}
	
	protected XWPFDocument initDocument() throws IOException {
		Resource excelFormat = new ClassPathResource("/document/DocxFormat.docx");
		XWPFDocument document = new XWPFDocument(excelFormat.getInputStream());
		return document;
	}
	
	protected void addParagraph(XWPFDocument document, String text) {
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setText(text);
		run.setFontSize(14);
	}

}
