package com.pwc.wechat.service.hr.report;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.hr.IReportConfigCrud;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.util.JsonUtil;

@Service("reportService")
public class SearchReportConfig implements IReportConfigSearch, IReportConfigUpdate {
	
	@Autowired
	private IReportConfigCrud iReportConfigCrud;

	@Override
	public void updateReportConfig(ReportConfig reportConfig) {
		reportConfig.setConfigsJson(JsonUtil.toNormalJson(reportConfig.getConfigs()));
		iReportConfigCrud.save(reportConfig);
	}

	@Override
	public List<ReportConfig> searchReportColumn() {
		List<ReportConfig> reportConfigs = new ArrayList<>();
		iReportConfigCrud.findAll().forEach((ReportConfig reportConfig) -> {
			reportConfig.setConfigs(JsonUtil.toNormalObject(reportConfig.getConfigsJson(), ReportColumn[].class));
			reportConfigs.add(reportConfig);
		});
		return reportConfigs;
	}
	
}
