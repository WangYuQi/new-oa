package com.pwc.wechat.service.hr.user;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.user.Avatar;
import com.pwc.wechat.entity.user.User;

public interface IUserHandler {
	
	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public void insertUser(User user);
    
	@Transactional(readOnly = true, rollbackFor = Throwable.class)
	public boolean verifyPassword(User user);
	
	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public void changePassword(User user);
	
	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public void saveAvatar(Avatar avatar);
	
	@Transactional(readOnly = true, rollbackFor = Throwable.class)
	public Avatar getAvatar(String userName);
}
