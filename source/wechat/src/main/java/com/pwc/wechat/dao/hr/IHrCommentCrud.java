package com.pwc.wechat.dao.hr;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.hr.HrComment;

public interface IHrCommentCrud extends JpaRepository<HrComment, String> {

}
