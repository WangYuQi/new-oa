package com.pwc.wechat.controller.candidate;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/applyOnPC")
public class ApplyOnPCController {
	
	private static final Logger logger = LogManager.getLogger(ApplyOnPCController.class);
	
	@RequestMapping(value = { "", "/" }, produces = "text/html; charset=utf-8")
	public ModelAndView visitCandidatePage() throws IOException {
		logger.info("Applying on PC");
		ModelAndView applyingonPC = new ModelAndView("candidatePC/mainpage");
		return applyingonPC;
	}
}