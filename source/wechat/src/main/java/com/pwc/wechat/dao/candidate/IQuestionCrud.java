package com.pwc.wechat.dao.candidate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.Questions;

public interface IQuestionCrud extends JpaRepository<Questions, Long>{

}
