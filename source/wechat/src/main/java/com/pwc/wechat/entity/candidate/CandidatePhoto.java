package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "candidate_photo", schema = "wechat")

public class CandidatePhoto implements Serializable {

	private static final long serialVersionUID = -9043503091411586394L;
	
	@Id
	@Column(name = "national_id")
	private String nationalId;
	
	@Column(name = "image")
	private byte[] image;
	
	public String getNationaId(){
		return this.nationalId;
	}
	
	public void setNationalId(String nationalId){
		this.nationalId = nationalId;
	}
	
	public byte[] getImage(){
		return image;
	}
	
	public void setImage(byte[] image){
		this.image = image;
	}
       
}
